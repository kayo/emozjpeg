target := libturbojpeg.js index.js convert.js
binary := convert.js

coffee := $(CURDIR)/node_modules/.bin/coffee

jpeg_library := extern/mozjpeg/libturbojpeg.a
jpeg_exports := tjInitCompress tjCompress2 tjGetErrorStr

test_samples := 

empty :=
space := $(empty) $(empty)
coma := ,

build: $(target)

define test

	@./convert.js -o $(2) -q $(3) -s $(4) -f $(5) samples/$(1)
	@echo "source:$(1) quality:$(3) sampling:$(4) flags:$(5) ratio:$$(echo `wc -c < test.jpg`*100/`wc -c < hazuki.jpg` | bc)%"
endef

test: $(target)
	$(foreach image,$(test_samples),$(foreach quality,90 80 70 50 0,$(foreach sampling,444 422 420 gray 440 411,$(foreach flags,bottomup fastupsample fastdct accuratedct,$(call test,$(image),test.jpg,$(quality),$(sampling),$(flags))))))
	rm -f test.jpg

%.js: %.coffee
	$(if $(filter $@,$(binary)),printf "#!/usr/bin/env node\n\n" > $@)
	$(coffee) --stdio -c < $^ >> $@
	$(if $(filter $@,$(binary)),chmod 755 $@)

$(jpeg_library):
	cd $(dir $@) && emconfigure cmake -DWITH_SIMD=FALSE && emmake make turbojpeg-static

libturbojpeg.js: $(jpeg_library)
	emcc -O2 --memory-init-file 0 --js-opts 0 $< -o $@ -s EXPORTED_FUNCTIONS='[$(subst $(space),$(coma),$(strip $(patsubst %,"_%",$(jpeg_exports))))]' -s TOTAL_MEMORY=33554432

clean:
	rm -f $(target)
