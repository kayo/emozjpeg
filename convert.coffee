Args = require "arg-parser"

{encode} = require "./index"

{readFileSync, writeFileSync} = require "fs"
Canvas = require "canvas"
{Image} = Canvas

#
# Preparing command-line
#

args = new Args "convert", "0.0.1", "mozjpeg image converter", "Convert images using mozjpeg"

args.add
  name: "source"
  desc: "source image"
  required: yes

args.add
  name: "output"
  desc: "output image"
  switches: [
    "-o"
    "--output"
  ]
  value: "image"

args.add
  name: "quality"
  desc: "jpeg quality (default: 70)"
  switches: [
    "-q"
    "--quality"
  ]
  value: "percent"

args.add
  name: "sampling"
  desc: "conversion sampling (default: 444)"
  switches: [
    "-s"
    "--sampling"
  ]
  value: samplingValues = "444,422,420,gray,440,411"

args.add
  name: "flags"
  desc: "coma-separated conversion flags (default: none)"
  switches: [
    "-f"
    "--flags"
  ]
  value: flagsValues = "bottomup,fastupsample,fastdct,accuratedct"

#
# Parsing arguments
#

return unless do args.parse

{params:{source, output, quality, sampling, flags}} = args

output ?= source
quality ?= 70
sampling ?= "444"
flags ?= ""

quality = quality|0

unless 0 <= quality <= 100
  console.error "Invalid quality: #{quality}! Please use decimal number in range 0...100"
  return

if sampling not in samplingValues.split ","
  console.error "Invalid sampling: #{sampling}! Please use one of #{samplingValues}."
  return

for flag in flags.split "," when flag and flag not in flagsValues.split ","
  console.error "Invalid flag: #{flag}! Please use any of #{flagsValues}."
  return

sampling = do sampling.toUpperCase
flags = do flags.toUpperCase

#
# Converting image
#

canvas = new Canvas 100, 100
context = canvas.getContext "2d"

image = new Image
image.src = readFileSync source
{width, height} = image

canvas.width = width
canvas.height = height
context.drawImage image, 0, 0, width, height

{data} = context.getImageData 0, 0, width, height

result = encode {data, width, height, quality, sampling, flags}

writeFileSync output, result
