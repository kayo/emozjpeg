Emscripten mozjpeg port
=======================

Port of [MozJPEG][mozjpeg] project to Emscripten.

Browser usage
-------------

Example using [Browserify][browserify] tool:

```coffeescript
# import encoder
{encode} = require "emozjpeg"

# create image object
image = new Image

image.onload = ->
  # get image size
  {width, height} = image

  # create canvas and set size
  canvas = document.createElement "canvas"
  canvas.width = width
  canvas.height = height

  # get drawing context
  context = canvas.getContext "2d"
  # draw image to canvas
  context.drawImage image, 0, 0, width, height

  # get raw image data
  {data} = context.getImageData 0, 0, width, height

  # encode image
  result = encode {data, width, height, quality, sampling, flags}

  # load data to blob
  blob = new Blob [result], type: 'text/html'
  # create object url
  url = URL.createObjectURL blob

  # load encoded image
  image.src = url

# load original image
image.src = "path/to/originalImage.jpg"
```

NodeJS usage
------------

```
npm install emozjpeg
```

```coffeescript
# import encoder
{encode} = require "emozjpeg"

# import file utils
{readFileSync, writeFileSync} = require "fs"

# import canvas library
Canvas = require "canvas"
{Image} = Canvas

# create image
image = new Image
# load image
image.src = readFileSync "originalImage.jpg"

# get image size
{width, height} = image

# create canvas and set size
canvas = new Canvas 100, 100
canvas.width = width
canvas.height = height

# get drawing context
context = canvas.getContext "2d"
# draw image to canvas
context.drawImage image, 0, 0, width, height

# get raw image data
{data} = context.getImageData 0, 0, width, height

# encode image
result = encode {data, width, height, quality, sampling, flags}

# write result to file
writeFileSync "convertedImage.jpg", result
```

Command-line tool
-----------------

```
usage: convert [options] <source>

 <source>                                                source image

 -f, --flags=BOTTOMUP,FASTUPSAMPLE,FASTDCT,ACCURATEDCT   coma-separated conversion flags (default: none)
 -h, --help                                              display help & usage
 -o, --output=IMAGE                                      output image
 -q, --quality=PERCENT                                   jpeg quality (default: 70)
 -s, --sampling=444,422,420,GRAY,440,411                 conversion sampling (default: 444)
 -v, --version                                           display cli name & version

 Convert images using mozjpeg
```

-------

[mozjpeg]: https://github.com/mozilla/mozjpeg
[browserify]: http://browserify.org/
