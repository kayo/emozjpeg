{cwrap, getValue, setValue, HEAPU8, _malloc, _free, i8, i32} = require "./libturbojpeg"

TJPF =
  RGB: 0
  BGR: 1
  RGBX: 2
  BGRX: 3
  XBGR: 4
  XRGB: 5
  GRAY: 6
  RGBA: 7
  BGRA: 8
  ABGR: 9
  ARGB: 10
  CMYK: 11

TJSAMP =
  444: 0
  422: 1
  420: 2
  GRAY: 3
  440: 4
  411: 5

TJFLAG =
  BOTTOMUP: 2
  FASTUPSAMPLE: 256
  NOREALLOC: 1024
  FASTDCT: 2048
  ACCURATEDCT: 4096

tjInitCompress = cwrap 'tjInitCompress', 'number'
tjCompress2 = cwrap 'tjCompress2', 'number', ['number', 'array', 'number', 'number', 'number', 'number', 'number', 'number', 'number', 'number', 'number']
tjGetErrorStr = cwrap 'tjGetErrorStr', 'string'

getFlags = (flags)->
  flags = flags.split /[+,|]/ if typeof flags is "string"
  result = 0
  result |= TJFLAG[flag] for flag in flags when flag of TJFLAG
  result

_handle = null

@encode = ({data, width, height, quality, pixelFormat, subSampling, flags})->
  _handle ?= do tjInitCompress
  
  quality ?= 100
  pixelFormat ?= "RGBA"
  subSampling ?= "444"
  flags = ["FASTDCT", "FASTUPSAMPLE"]
    
  dst_buf_ptr = _malloc 4
  setValue dst_buf_ptr, 0, i32
    
  dst_buf_len = _malloc 4
  setValue dst_buf_len, 0, i32

  unless 0 is tjCompress2 _handle, data, width, width * 4, height, TJPF[pixelFormat], dst_buf_ptr, dst_buf_len, TJSAMP[subSampling], quality, getFlags flags
    throw new Error do tjGetErrorStr
  
  dst_len = (new Uint32Array HEAPU8.buffer, dst_buf_len)[0]
    
  dst_buf = (new Uint32Array HEAPU8.buffer, dst_buf_ptr)[0]
  dst_data = new Buffer new Uint8Array HEAPU8.buffer, dst_buf, dst_len
    
  _free dst_buf_len
  _free dst_buf
  _free dst_buf_ptr
    
  dst_data
